use serde_derive::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct FeedlyError {
    #[serde(rename = "errorCode")]
    pub error_code: u32,
    #[serde(rename = "errorId")]
    pub error_id: String,
    #[serde(rename = "errorMessage")]
    pub error_message: String,
}

impl PartialEq for FeedlyError {
    fn eq(&self, other: &FeedlyError) -> bool {
        self.error_code == other.error_code
    }
}

impl Eq for FeedlyError {}
