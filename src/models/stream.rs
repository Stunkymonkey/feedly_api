use super::entry::{Entry, Link};
use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Stream {
    pub id: String,
    #[serde(default)]
    pub title: Option<String>,
    #[serde(default)]
    pub direction: Option<String>,
    #[serde(default)]
    pub updated: Option<u64>,
    #[serde(default)]
    pub alternate: Option<Vec<Link>>,
    #[serde(default)]
    pub continuation: Option<String>,
    pub items: Vec<Entry>,
}

impl Stream {
    pub fn decompose(
        self,
    ) -> (
        String,
        Option<String>,
        Option<String>,
        Option<u64>,
        Option<Vec<Link>>,
        Option<String>,
        Vec<Entry>,
    ) {
        (
            self.id,
            self.title,
            self.direction,
            self.updated,
            self.alternate,
            self.continuation,
            self.items,
        )
    }
}
