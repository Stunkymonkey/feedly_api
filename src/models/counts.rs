use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Counts {
    pub unreadcounts: Vec<Count>,
    pub updated: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Count {
    pub id: String,
    pub count: u32,
    pub updated: u64,
}
